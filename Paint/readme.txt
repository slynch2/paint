Paint by Levi Shutts, Danny Lu, Syd Lynch


This is an illustrator application with small network capabilities. Pressing and holding the left mouse button paints
shapes to the canvas depending on which brush is selected. Pressing and holding right click will erase the canvas in the
shape of whatever is selected. Adjusting the slider scales the size of the brush. To save the canvas, press the "Save Image"
button. The drawing will be saved as "masterpiece.png" in the directory of the application.

To compile and run:

*You can start the app without connecting to the server, but here are the instructions to start with the server*
*If you aren't interested in using the server just move to the "For the Client" section*

For the Server:
1. g++ -std=c++11 -c MultiServer.cpp

2. g++ -o MultiServer MultiServer.o -lsfml-graphics -lsfml-window -lsfml-system -lsfml-network

3. ./MultiServer

*Once MultiServer is running, open another Terminal to set up for the Client*
*To Close the MultiServer, Type: Ctrl^c*

For the Client:
1. g++ -std=c++11 -c main.cpp
   g++ -std=c++11 -c brush.cpp 
   g++ -std=c++11 -c UserInterface.cpp

2. g++ -o paint main.o brush.o UserInterface.o -ltgui -lsfml-network -lsfml-graphics -lsfml-window -lsfml-system

3. ./paint
